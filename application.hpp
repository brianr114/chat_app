/*
* (c) Copyright, Real-Time Innovations, 2020.  All rights reserved.
* RTI grants Licensee a license to use, modify, compile, and create derivative
* works of the software solely for use with RTI Connext DDS. Licensee may
* redistribute copies of the software provided that all such copies are subject
* to this license. The software is provided "as is", with no warranty of any
* type, including any warranty for fitness for any purpose. RTI is under no
* obligation to maintain or support the software. RTI shall not be liable for
* any incidental or consequential damages arising out of the use or inability
* to use the software.
*/

#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <iostream>
#include <csignal>
#include <dds/core/ddscore.hpp>

namespace application {

    // Catch control-C and tell application to shut down
    bool shutdown_requested = false;

    inline void stop_handler(int)
    {
        shutdown_requested = true;
        std::cout << "preparing to shut down..." << std::endl;
    }

    inline void setup_signal_handlers()
    {
        signal(SIGINT, stop_handler);
        signal(SIGTERM, stop_handler);
    }

    enum class ParseReturn {
        ok,
        failure,
        exit
    };

    struct ApplicationArguments {
        ParseReturn parse_result;
        unsigned int domain_id;
        rti::config::Verbosity verbosity;
        std::string user_name;
        std::string group_name;
        std::string first_name;
        std::string last_name;
        bool enable_debug;

        ApplicationArguments(
            ParseReturn parse_result_param,
            unsigned int domain_id_param,
            rti::config::Verbosity verbosity_param,
            std::string user_name_param,
            std::string group_name_param,
            std::string first_name_param,
            std::string last_name_param,
            bool enable_debug_param)
            : parse_result(parse_result_param),
            domain_id(domain_id_param),
            verbosity(verbosity_param),
            user_name(user_name_param),
            group_name(group_name_param),
            first_name(first_name_param),
            last_name(last_name_param),
            enable_debug(enable_debug_param){}
    };

    inline void set_verbosity(
        rti::config::Verbosity& verbosity,
        int verbosity_value)
    {
        switch (verbosity_value) {
            case 0:
            verbosity = rti::config::Verbosity::SILENT;
            break;
            case 1:
            verbosity = rti::config::Verbosity::EXCEPTION;
            break;
            case 2:
            verbosity = rti::config::Verbosity::WARNING;
            break;
            case 3:
            verbosity = rti::config::Verbosity::STATUS_ALL;
            break;
            default:
            verbosity = rti::config::Verbosity::EXCEPTION;
            break;
        }
    }

    // Parses application arguments for example.
    inline ApplicationArguments parse_arguments(int argc, char *argv[])
    {
        int arg_processing = 1;
        bool show_usage = false;
        ParseReturn parse_result = ParseReturn::ok;
        unsigned int domain_id = 0;
        rti::config::Verbosity verbosity(rti::config::Verbosity::EXCEPTION);
        std::string user_name;
        std::string group_name, first_name, last_name;
        bool enable_debug = false;

        while (arg_processing < argc) {
            if ((argc > arg_processing + 1) 
            && (strcmp(argv[arg_processing], "-d") == 0
            || strcmp(argv[arg_processing], "--domain") == 0)) {
                domain_id = atoi(argv[arg_processing + 1]);
                arg_processing += 2;
            } else if ((argc > arg_processing + 1)
            && (strcmp(argv[arg_processing], "-v") == 0
            || strcmp(argv[arg_processing], "--verbosity") == 0)) {
                set_verbosity(verbosity, atoi(argv[arg_processing + 1]));
                arg_processing += 2;
            } else if ((argc > arg_processing + 1) 
                && strcmp(argv[arg_processing], "-u") == 0
                || strcmp(argv[arg_processing], "--user") == 0) {
                user_name = std::string(argv[arg_processing + 1]);
                arg_processing += 2;
            } else if ((argc > arg_processing + 1)
                && strcmp(argv[arg_processing], "-g") == 0
                || strcmp(argv[arg_processing], "--group") == 0) {
                group_name = std::string(argv[arg_processing + 1]);
                arg_processing += 2;
            } else if ((argc > arg_processing + 1)
                && strcmp(argv[arg_processing], "-f") == 0
                || strcmp(argv[arg_processing], "--firstname") == 0) {
                first_name = std::string(argv[arg_processing + 1]);
                arg_processing += 2;
            } else if ((argc > arg_processing + 1)
                && strcmp(argv[arg_processing], "-l") == 0
                || strcmp(argv[arg_processing], "--lastname") == 0) {
                last_name = std::string(argv[arg_processing + 1]);
                arg_processing += 2;
            } else if (strcmp(argv[arg_processing], "-d") == 0
                || strcmp(argv[arg_processing], "--debug") == 0) {
                enable_debug = true;
                arg_processing += 1;
            } else if (strcmp(argv[arg_processing], "-h") == 0
                || strcmp(argv[arg_processing], "--help") == 0) {
                std::cout << "Example application." << std::endl;
                show_usage = true;
                parse_result = ParseReturn::exit;
                break;
            }
            else {
                std::cout << "Bad parameter." << std::endl;
                show_usage = true;
                parse_result = ParseReturn::failure;
                break;
            }
        }
        if (show_usage) {
            std::cout << "Usage:\n"\
            "    -d, --domain       <int>   Domain ID this application will\n" \
            "                               subscribe in.  \n"
            "                               Default: 0\n"\
            "    -v, --verbosity    <int>   How much debugging output to show.\n"\
            "                               Range: 0-3 \n"
            "                               Default: 1"
            << std::endl;
        }

        return ApplicationArguments(parse_result, domain_id, verbosity, 
            user_name, group_name, first_name, last_name, enable_debug);
    }

}  // namespace application

#endif  // APPLICATION_HPP
