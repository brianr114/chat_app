/*
* (c) Copyright, Real-Time Innovations, 2020.  All rights reserved.
* RTI grants Licensee a license to use, modify, compile, and create derivative
* works of the software solely for use with RTI Connext DDS. Licensee may
* redistribute copies of the software provided that all such copies are subject
* to this license. The software is provided "as is", with no warranty of any
* type, including any warranty for fitness for any purpose. RTI is under no
* obligation to maintain or support the software. RTI shall not be liable for
* any incidental or consequential damages arising out of the use or inability
* to use the software.
*/

#include <iostream>
#include <thread>
#include <unordered_map>
#include <mutex>

#include <dds/pub/ddspub.hpp>
#include <dds/sub/ddssub.hpp>
#include <rti/util/util.hpp>      // for sleep()
#include <rti/config/Logger.hpp>  // for logging
// alternatively, to include all the standard APIs:
//  <dds/dds.hpp>
// or to include both the standard APIs and extensions:
//  <rti/rti.hpp>
//
// For more information about the headers and namespaces, see:
//    https://community.rti.com/static/documentation/connext-dds/6.1.0/doc/api/connext_dds/api_cpp2/group__DDSNamespaceModule.html
// For information on how to use extensions, see:
//    https://community.rti.com/static/documentation/connext-dds/6.1.0/doc/api/connext_dds/api_cpp2/group__DDSCpp2Conventions.html

#include "application.hpp"  // for command line parsing and ctrl-c
#include "chat_app.hpp"

#define MAX_TRIES 10
#define NUM_WAITSETS 3
#define DEBUG false

/* Because all domain participants, including non-chat participants (e.g. Admin Console) are saved in the
 * user list, the valid_user boolean delineates a chat participant from a non-chat participant*/
struct USER_DATA{
    std::string pid;
    std::string ip_addr;
    user user;
    bool valid_user;
};

/* Globals */
std::unordered_map<std::string, USER_DATA> users_list;
std::mutex users_mutex;
int waitsets_active{ 0 };
std::mutex waitsets_mutex;

/* Main Thread */
void run_chat_app(application::ApplicationArguments args);
message parse_instring(std::string instring, std::string my_id);
void print_users();

/* Incoming Users Thread */
void process_users_thread(dds::sub::DataReader<user> usersReader, bool print_debug);
void process_users(dds::sub::DataReader<user> usersReader, bool print_debug);

/* Builtin Topics Thread */
void builtin_participant_thread(dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData> participantReader);
void process_participants(dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData> participantReader);

/* Incoming Messages Thread*/
void process_messages(dds::sub::DataReader<message> messagesReader);
void display_incoming_messages(dds::sub::DataReader<message> messagesReaderader);

/* Utilities */
std::string parse_instance_handle(dds::core::InstanceHandle &handle);
std::string create_ip_string(dds::core::ByteSeq address);

/*=================================MAIN_THREAD=====================================================*/

int main(int argc, char *argv[])
{
    using namespace application;

    // Parse arguments and handle control-C
    auto arguments = parse_arguments(argc, argv);
    if (arguments.parse_result == ParseReturn::exit) {
        return EXIT_SUCCESS;
    } else if (arguments.parse_result == ParseReturn::failure) {
        return EXIT_FAILURE;
    }
    setup_signal_handlers();

    // Sets Connext verbosity to help debugging
    rti::config::Logger::instance().verbosity(arguments.verbosity);

    try {
        run_chat_app(arguments);
    } catch (const std::exception& ex) {
        // This will catch DDS exceptions
        std::cerr << "Exception in run_chat_app(): " << ex.what()
        << std::endl;
        return EXIT_FAILURE;
    }

    // Releases the memory used by the participant factory.  Optional at
    // application exit
    dds::domain::DomainParticipant::finalize_participant_factory();

    return EXIT_SUCCESS;
}

void run_chat_app(application::ApplicationArguments args)
{
    // DDS objects behave like shared pointers or value types
    // (see https://community.rti.com/best-practices/use-modern-c-types-correctly)

    // Open custom XML QoS file
    dds::core::QosProvider qos_provider("chat_app.xml");

    // Disable autoenable_created_entities for domain participant factory
    dds::domain::qos::DomainParticipantFactoryQos qos;
    qos << dds::core::policy::EntityFactory::ManuallyEnable();
    dds::domain::DomainParticipant::participant_factory_qos(qos);

    // Create domain participant
    dds::domain::DomainParticipant participant(
        args.domain_id,
        qos_provider.participant_qos("chat_app_qos_library::chat_app_profile::domain_participant_qos"));

    // Create topics
    dds::topic::Topic<message> messages_topic(participant, "messages");
    dds::topic::Topic<user> users_topic(participant, "users");
    
    // Create publishers
    dds::pub::Publisher publisher(participant);

    // Create subscribers
    dds::sub::Subscriber subscriber(participant);
    
    // Set up content filtering on message topic (user_id == args.user_name || group_name == args.group_name)
    std::vector<std::string> messages_filter_strings = {
        "'" + args.user_name + "'",
        "'" + args.group_name + "'" };
    dds::topic::Filter messages_filter(
        "to_id = %0 OR (to_id = %1 AND from_id <> %0)",
        messages_filter_strings);
    //messages_filter->name(rti::topic::stringmatch_filter_name());  // TODO: What does this do and why does it break me?
    dds::topic::ContentFilteredTopic<message> cft_messages_topic = dds::topic::ContentFilteredTopic<message>(
        messages_topic,
        "messages_cft",
        messages_filter);
    dds::sub::DataReader<message> messagesReader(
        subscriber,
        cft_messages_topic,
        qos_provider.datareader_qos("chat_app_qos_library::messages"));

    // Set up content filtering on users topic (user_name != args.user_id)
    std::vector<std::string> users_filter_strings = { "'" + args.user_name + "'"};
    dds::topic::Filter users_filter("user_id <> %0", users_filter_strings);
    dds::topic::ContentFilteredTopic<user> cft_users_topic = dds::topic::ContentFilteredTopic<user>(
        users_topic,
        "users_cft",
        users_filter);
    dds::sub::DataReader<user> usersReader(
        subscriber,
        cft_users_topic,
        qos_provider.datareader_qos("chat_app_qos_library::users"));

    // Installing listeners for the builtin topics requires several steps
    // First get the builtin subscriber.
    dds::sub::Subscriber builtin_subscriber =
        dds::sub::builtin_subscriber(participant);
    
    // Then get builtin subscriber's datareader for participants.
    std::vector<dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData>>
        participant_reader;
    dds::sub::find<dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData>>(
        builtin_subscriber,
        dds::topic::participant_topic_name(),
        std::back_inserter(participant_reader));

    // Create data writers
    dds::pub::DataWriter<message> messagesWriter(publisher, messages_topic, qos_provider.datawriter_qos(
        "chat_app_qos_library::messages"));
    dds::pub::DataWriter<user> usersWriter(publisher, users_topic, qos_provider.datawriter_qos(
        "chat_app_qos_library::users"));

    // Launch threads
    std::thread builtin_participant_thread(builtin_participant_thread, participant_reader[0]);
    std::thread user_thread(process_users_thread, usersReader, args.enable_debug);
    std::thread message_thread(display_incoming_messages, messagesReader);

    // Wait for waitsets in threads to get enabled
    int temp_waitsets_counter{ 0 };

    for (int i = 0; i < MAX_TRIES; i++) {
        waitsets_mutex.lock();
        temp_waitsets_counter = waitsets_active;
        waitsets_mutex.unlock();
        if (temp_waitsets_counter == NUM_WAITSETS)
            break;
        if (DEBUG) std::cout << "Waiting for theads to launch\n" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }

    if (temp_waitsets_counter != NUM_WAITSETS) {
        if (DEBUG) std::cout << "Waitsets not active after 10 tries" << std::endl;
        // Throw an exception for completeness
    }

    // Enable DDS entities
    participant.enable();

    /* Could call ignore() here to prevent datareaders from receiving topics sent by
     * datawriters in the same domain participant */

    // Announce self to world
    user my_user_data(args.user_name, args.first_name, args.last_name, args.group_name);
    usersWriter.write(my_user_data);

    std::cout << std::endl;

    message data;
    std::string buffer;

    // Command line loop
    while (!application::shutdown_requested) {
        std::cout << "Enter Command: ";
        std::getline(std::cin, buffer);

        if (buffer.find("send") == 0) {
            data = parse_instring(buffer, args.user_name);
            messagesWriter.write(data);
            std::cout << std::endl;
        }
        else if (buffer.compare("list") == 0) {
            print_users();
        }
        else if (buffer.compare("exit") == 0) {
            application::shutdown_requested = true;
        }
        else
            printf("Invalid Command\n\n");
    }

    // Wait on threads to complete
    message_thread.join();
    user_thread.join();
    builtin_participant_thread.join();

    // Dispose users write so other readers know chat use has dropped off
    usersWriter.dispose_instance(usersWriter.lookup_instance(my_user_data));
}

message parse_instring(std::string instring, std::string my_id)
{
    message data;
    size_t first_space, second_space;

    // Expect data in format "send [TO_ID] [MESSAGE]"
    first_space = instring.find(' ', 0);
    second_space = instring.find(' ', first_space + 1);
    
    data.from_id(my_id);
    data.to_id(instring.substr(first_space + 1, second_space - (first_space + 1)));
    data.msg(instring.substr(second_space + 1));

    return data;
}

void print_users()
{
    bool suffix_eol = false;

    std::cout << std::endl;

    users_mutex.lock();
    if (DEBUG) std::cout << "Size of users list: " << users_list.size() << std::endl;

    // Iterate through users_list.  If a user is valid (e.g. not Admin Console) print their data
    for (const auto& x : users_list) {
        if (DEBUG) std::cout << "User Key: " << x.first << std::endl;
        if (x.second.valid_user) {
            std::cout << x.second.user << std::endl;
            suffix_eol = true;
        }
    }
    users_mutex.unlock();

    if(suffix_eol)
        std::cout << std::endl;
}

/*=================================PROCESS_USERS_THREAD=====================================================*/

void process_users_thread(dds::sub::DataReader<user> usersReader, bool print_debug)
{
    // Create a ReadCondition for any data received on this reader and set a
    // handler to process the data
    dds::sub::cond::ReadCondition read_condition(
        usersReader,
        dds::sub::status::DataState::any(),
        [usersReader, print_debug]() {process_users(usersReader, print_debug); });

    // WaitSet will be woken when the attached condition is triggered
    dds::core::cond::WaitSet waitset;
    waitset += read_condition;

    waitsets_mutex.lock();
    waitsets_active++;
    waitsets_mutex.unlock();

    while (!application::shutdown_requested) {
        // Run the handlers of the active conditions. Wait for up to 1 second.
        waitset.dispatch(dds::core::Duration(1));
    }
}

void process_users(dds::sub::DataReader<user> usersReader, bool print_debug)
{
    USER_DATA temp_user_data;
    std::string key;

    if(print_debug)
        std::cout << std::endl;

    // Take all samples
    dds::sub::LoanedSamples<user> samples = usersReader.take();
    
    for (const auto& sample : samples) {
        key = parse_instance_handle(sample.info().publication_handle());
        if (sample.info().valid()) {            
            for (int i = 0; i < MAX_TRIES; i++){
                users_mutex.lock();
                if (users_list.find(key) == users_list.end()) {
                    // User not found yet, release lock, sleep, and try again
                    users_mutex.unlock();
                    if(DEBUG) std::cout << "User Thread - " << key << " not found in iteration: " << i << std::endl;
                    std::this_thread::sleep_for(std::chrono::milliseconds(500));
                    continue;
                }
                
                // User is found in users_list...add their user data to the list
                temp_user_data = users_list[key];
                temp_user_data.user = sample.data();
                temp_user_data.valid_user = true;
                users_list[key] = temp_user_data;
                users_mutex.unlock();

                if (print_debug)
                    std::cout << "Discovered " << temp_user_data.user.user_id()
                    << " running chat app in IP " << temp_user_data.ip_addr
                    << " with process ID " << temp_user_data.pid << std::endl;

                break;
            }
        }
        else {
            // Invalid sample received...means user is leaving chat...remove them from users_list
            users_mutex.lock();
            if (DEBUG) std::cout << "Size of Users List Before Delete: " << users_list.size() << std::endl;
            if(print_debug)
                std::cout << users_list[key].user.user_id() << " left the chat." << std::endl;
            users_list.erase(key);
            if (DEBUG) std::cout << "Size of Users List After Delete: " << users_list.size() << std::endl;
            users_mutex.unlock();
        }
    }
}

/*=====================================INCOMING_MESSAGE_THREAD========================================*/

void display_incoming_messages(dds::sub::DataReader<message> messagesReader)
{
    // Create a ReadCondition for any data received on this reader and set a
    // handler to process the data
    dds::sub::cond::ReadCondition read_condition(
        messagesReader,
        dds::sub::status::DataState::any(),
        [messagesReader]() {process_messages(messagesReader); });

    // WaitSet will be woken when the attached condition is triggered
    dds::core::cond::WaitSet waitset;
    waitset += read_condition;

    waitsets_mutex.lock();
    waitsets_active++;
    waitsets_mutex.unlock();

    while (!application::shutdown_requested) {
        // Run the handlers of the active conditions. Wait for up to 1 second.
        waitset.dispatch(dds::core::Duration(1));
    }
}

void process_messages(dds::sub::DataReader<message> messagesReader)
{
    bool prefix_endl = false,  suffix_endl = false;

    // Take all messages, and print each valid message to console
    dds::sub::LoanedSamples<message> samples = messagesReader.take();
    for (const auto& sample : samples) {
        if (sample.info().valid()) {
            if (!prefix_endl) {
                std::cout << std::endl << std::endl;
                prefix_endl = true;
            }
            std::cout << sample.data() << std::endl;
            suffix_endl = true;
         }
    }

    if (suffix_endl)
        std::cout << std::endl;
} // The LoanedSamples destructor returns the loan

/*=================================NEW_PARTICIPANT_THREAD============================================*/

void builtin_participant_thread(dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData> participantReader)
{
    // Create a ReadCondition for any data received on this reader and set a
    // handler to process the data
    if (DEBUG) std::cout << "Participant Thread - Begin\n";

    dds::sub::cond::ReadCondition read_condition(
        participantReader,
        dds::sub::status::DataState::any(),
        [participantReader]() {process_participants(participantReader); });

    // WaitSet will be woken when the attached condition is triggered
    dds::core::cond::WaitSet waitset;
    waitset += read_condition;

    waitsets_mutex.lock();
    waitsets_active++;
    waitsets_mutex.unlock();

    while (!application::shutdown_requested) {
        // Run the handlers of the active conditions. Wait for up to 1 second.
        waitset.dispatch(dds::core::Duration(1));
    }

    if (DEBUG) std::cout << "Participant Thread - End\n";
}

void process_participants(dds::sub::DataReader<dds::topic::ParticipantBuiltinTopicData> participantReader)
{
    std::pair<std::string, USER_DATA> element;
    std::string key;
    
    // Since builtin topic, read(), not take()
    dds::sub::LoanedSamples<dds::topic::ParticipantBuiltinTopicData> samples =
        participantReader./*select().state(dds::sub::status::DataState::new_instance()).*/take();

    if(DEBUG) std::cout << "\nParticipant Thread - Samples Read: " << samples.length() << std::endl;

    // Add all valid samples to users_list.  Check to ensure not already in user_list
    users_mutex.lock();
    for (const auto& sample : samples) {
        key = parse_instance_handle(sample.info().instance_handle());
        if (sample.info().valid()) {
            if (users_list.find(key) == users_list.end()) {  // Not presently in users_list
                element.first = key;
                element.second.ip_addr = create_ip_string(sample.data()->default_unicast_locators()[0].address());
                element.second.pid = sample.data()->property().get("dds.sys_info.process_id");
                element.second.valid_user = false;
                users_list.insert(element);
                if (DEBUG) std::cout << "Participant Thread - New Key: " << key << std::endl;
            }
            else
                if (DEBUG) std::cout << "Participant Thread - Valid Sample - Key Already in List: " << key << std::endl;
        }
        else if(users_list.find(key) != users_list.end())
            if (DEBUG) std::cout << "Particiapnt Thread - Invalid Sample - Key Already in List: " << key << std::endl;
    }
    users_mutex.unlock();
    if(DEBUG) std::cout << "Participant Thread - Exit processing function\n";
}
/*====================================UTILITIES=====================================================*/

std::string parse_instance_handle(dds::core::InstanceHandle &handle)
{
    std::stringstream ss;
    ss << handle;
    std::string handle_string = ss.str().substr(0,24);
    return handle_string;
}

std::string create_ip_string(dds::core::ByteSeq address)
{
    std::stringstream ss;
    ss << static_cast<int>(address[12]) << "."
       << static_cast<int>(address[13]) << "."
       << static_cast<int>(address[14]) << "."
       << static_cast<int>(address[15]);
    return ss.str();
}