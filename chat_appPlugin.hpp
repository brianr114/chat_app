

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from chat_app.idl
using RTI Code Generator (rtiddsgen) version 3.1.0.
The rtiddsgen tool is part of the RTI Connext DDS distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the Code Generator User's Manual.
*/

#ifndef chat_appPlugin_1835167378_h
#define chat_appPlugin_1835167378_h

#include "chat_app.hpp"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

/* The type used to store keys for instances of type struct
* AnotherSimple.
*
* By default, this type is struct user
* itself. However, if for some reason this choice is not practical for your
* system (e.g. if sizeof(struct user)
* is very large), you may redefine this typedef in terms of another type of
* your choosing. HOWEVER, if you define the KeyHolder type to be something
* other than struct AnotherSimple, the
* following restriction applies: the key of struct
* user must consist of a
* single field of your redefined KeyHolder type and that field must be the
* first field in struct user.
*/
typedef  class user userKeyHolder;

#define userPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample

#define userPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define userPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer

#define userPlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
#define userPlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

#define userPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define userPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern user*
userPluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern user*
userPluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern user*
userPluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
userPluginSupport_copy_data(
    user *out,
    const user *in);

NDDSUSERDllExport extern void 
userPluginSupport_destroy_data_w_params(
    user *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
userPluginSupport_destroy_data_ex(
    user *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
userPluginSupport_destroy_data(
    user *sample);

NDDSUSERDllExport extern void 
userPluginSupport_print_data(
    const user *sample,
    const char *desc,
    unsigned int indent);

NDDSUSERDllExport extern user*
userPluginSupport_create_key_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern user*
userPluginSupport_create_key(void);

NDDSUSERDllExport extern void 
userPluginSupport_destroy_key_ex(
    userKeyHolder *key,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
userPluginSupport_destroy_key(
    userKeyHolder *key);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
userPlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
userPlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
userPlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
userPlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
userPlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    user *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
userPlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    user *out,
    const user *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool
userPlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const user *sample,
    ::dds::core::policy::DataRepresentationId representation
    = ::dds::core::policy::DataRepresentation::xcdr()); 

NDDSUSERDllExport extern RTIBool 
userPlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    user **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
userPlugin_deserialize_from_cdr_buffer(
    user *sample,
    const char * buffer,
    unsigned int length);    

NDDSUSERDllExport extern unsigned int 
userPlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
userPlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
userPlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
userPlugin_get_serialized_key_max_size_for_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
userPlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    user ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
userPlugin_instance_to_key(
    PRESTypePluginEndpointData endpoint_data,
    userKeyHolder *key, 
    const user *instance);

NDDSUSERDllExport extern RTIBool 
userPlugin_key_to_instance(
    PRESTypePluginEndpointData endpoint_data,
    user *instance, 
    const userKeyHolder *key);

NDDSUSERDllExport extern RTIBool 
userPlugin_serialized_sample_to_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    DDS_KeyHash_t *keyhash,
    RTIBool deserialize_encapsulation,
    void *endpoint_plugin_qos); 

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
userPlugin_new(void);

NDDSUSERDllExport extern void
userPlugin_delete(struct PRESTypePlugin *);

/* The type used to store keys for instances of type struct
* AnotherSimple.
*
* By default, this type is struct message
* itself. However, if for some reason this choice is not practical for your
* system (e.g. if sizeof(struct message)
* is very large), you may redefine this typedef in terms of another type of
* your choosing. HOWEVER, if you define the KeyHolder type to be something
* other than struct AnotherSimple, the
* following restriction applies: the key of struct
* message must consist of a
* single field of your redefined KeyHolder type and that field must be the
* first field in struct message.
*/
typedef  class message messageKeyHolder;

#define messagePlugin_get_sample PRESTypePluginDefaultEndpointData_getSample

#define messagePlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define messagePlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer

#define messagePlugin_get_key PRESTypePluginDefaultEndpointData_getKey 
#define messagePlugin_return_key PRESTypePluginDefaultEndpointData_returnKey

#define messagePlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define messagePlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern message*
messagePluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern message*
messagePluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern message*
messagePluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
messagePluginSupport_copy_data(
    message *out,
    const message *in);

NDDSUSERDllExport extern void 
messagePluginSupport_destroy_data_w_params(
    message *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
messagePluginSupport_destroy_data_ex(
    message *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
messagePluginSupport_destroy_data(
    message *sample);

NDDSUSERDllExport extern void 
messagePluginSupport_print_data(
    const message *sample,
    const char *desc,
    unsigned int indent);

NDDSUSERDllExport extern message*
messagePluginSupport_create_key_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern message*
messagePluginSupport_create_key(void);

NDDSUSERDllExport extern void 
messagePluginSupport_destroy_key_ex(
    messageKeyHolder *key,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
messagePluginSupport_destroy_key(
    messageKeyHolder *key);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
messagePlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
messagePlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
messagePlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
messagePlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
messagePlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    message *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
messagePlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    message *out,
    const message *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool
messagePlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const message *sample,
    ::dds::core::policy::DataRepresentationId representation
    = ::dds::core::policy::DataRepresentation::xcdr()); 

NDDSUSERDllExport extern RTIBool 
messagePlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    message **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
messagePlugin_deserialize_from_cdr_buffer(
    message *sample,
    const char * buffer,
    unsigned int length);    

NDDSUSERDllExport extern unsigned int 
messagePlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
messagePlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
messagePlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
messagePlugin_get_serialized_key_max_size_for_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
messagePlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    message ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
messagePlugin_instance_to_key(
    PRESTypePluginEndpointData endpoint_data,
    messageKeyHolder *key, 
    const message *instance);

NDDSUSERDllExport extern RTIBool 
messagePlugin_key_to_instance(
    PRESTypePluginEndpointData endpoint_data,
    message *instance, 
    const messageKeyHolder *key);

NDDSUSERDllExport extern RTIBool 
messagePlugin_serialized_sample_to_keyhash(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    DDS_KeyHash_t *keyhash,
    RTIBool deserialize_encapsulation,
    void *endpoint_plugin_qos); 

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
messagePlugin_new(void);

NDDSUSERDllExport extern void
messagePlugin_delete(struct PRESTypePlugin *);

#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* chat_appPlugin_1835167378_h */

