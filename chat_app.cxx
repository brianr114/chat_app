

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from chat_app.idl
using RTI Code Generator (rtiddsgen) version 3.1.0.
The rtiddsgen tool is part of the RTI Connext DDS distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the Code Generator User's Manual.
*/

#include <iosfwd>
#include <iomanip>

#include "rti/topic/cdr/Serialization.hpp"

#include "chat_app.hpp"
#include "chat_appPlugin.hpp"

#include <rti/util/ostream_operators.hpp>

// ---- user: 

user::user() :
    m_user_id_ ("") ,
    m_first_name_ ("") ,
    m_last_name_ ("") ,
    m_group_name_ ("")  {
}   

user::user (
    const std::string& user_id,
    const std::string& first_name,
    const std::string& last_name,
    const std::string& group_name)
    :
        m_user_id_( user_id ),
        m_first_name_( first_name ),
        m_last_name_( last_name ),
        m_group_name_( group_name ) {
}

#ifdef RTI_CXX11_RVALUE_REFERENCES
#ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
user::user(user&& other_) OMG_NOEXCEPT  :m_user_id_ (std::move(other_.m_user_id_))
,
m_first_name_ (std::move(other_.m_first_name_))
,
m_last_name_ (std::move(other_.m_last_name_))
,
m_group_name_ (std::move(other_.m_group_name_))
{
} 

user& user::operator=(user&&  other_) OMG_NOEXCEPT {
    user tmp(std::move(other_));
    swap(tmp); 
    return *this;
}
#endif
#endif   

void user::swap(user& other_)  OMG_NOEXCEPT 
{
    using std::swap;
    swap(m_user_id_, other_.m_user_id_);
    swap(m_first_name_, other_.m_first_name_);
    swap(m_last_name_, other_.m_last_name_);
    swap(m_group_name_, other_.m_group_name_);
}  

bool user::operator == (const user& other_) const {
    if (m_user_id_ != other_.m_user_id_) {
        return false;
    }
    if (m_first_name_ != other_.m_first_name_) {
        return false;
    }
    if (m_last_name_ != other_.m_last_name_) {
        return false;
    }
    if (m_group_name_ != other_.m_group_name_) {
        return false;
    }
    return true;
}
bool user::operator != (const user& other_) const {
    return !this->operator ==(other_);
}

std::ostream& operator << (std::ostream& o,const user& sample)
{
    ::rti::util::StreamFlagSaver flag_saver (o);
    o <<"[";
    o << "user_id: " << sample.user_id()<<", ";
    o << "first_name: " << sample.first_name()<<", ";
    o << "last_name: " << sample.last_name()<<", ";
    o << "group_name: " << sample.group_name() ;
    o <<"]";
    return o;
}

// ---- message: 

message::message() :
    m_to_id_ ("") ,
    m_from_id_ ("") ,
    m_msg_ ("")  {
}   

message::message (
    const std::string& to_id,
    const std::string& from_id,
    const std::string& msg)
    :
        m_to_id_( to_id ),
        m_from_id_( from_id ),
        m_msg_( msg ) {
}

#ifdef RTI_CXX11_RVALUE_REFERENCES
#ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
message::message(message&& other_) OMG_NOEXCEPT  :m_to_id_ (std::move(other_.m_to_id_))
,
m_from_id_ (std::move(other_.m_from_id_))
,
m_msg_ (std::move(other_.m_msg_))
{
} 

message& message::operator=(message&&  other_) OMG_NOEXCEPT {
    message tmp(std::move(other_));
    swap(tmp); 
    return *this;
}
#endif
#endif   

void message::swap(message& other_)  OMG_NOEXCEPT 
{
    using std::swap;
    swap(m_to_id_, other_.m_to_id_);
    swap(m_from_id_, other_.m_from_id_);
    swap(m_msg_, other_.m_msg_);
}  

bool message::operator == (const message& other_) const {
    if (m_to_id_ != other_.m_to_id_) {
        return false;
    }
    if (m_from_id_ != other_.m_from_id_) {
        return false;
    }
    if (m_msg_ != other_.m_msg_) {
        return false;
    }
    return true;
}
bool message::operator != (const message& other_) const {
    return !this->operator ==(other_);
}

std::ostream& operator << (std::ostream& o,const message& sample)
{
    ::rti::util::StreamFlagSaver flag_saver (o);
    o <<"[";
    o << "to_id: " << sample.to_id()<<", ";
    o << "from_id: " << sample.from_id()<<", ";
    o << "msg: " << sample.msg() ;
    o <<"]";
    return o;
}

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        #ifndef NDDS_STANDALONE_TYPE
        template<>
        struct native_type_code< user > {
            static DDS_TypeCode * get()
            {
                using namespace ::rti::topic::interpreter;

                static RTIBool is_initialized = RTI_FALSE;

                static DDS_TypeCode user_g_tc_user_id_string;
                static DDS_TypeCode user_g_tc_first_name_string;
                static DDS_TypeCode user_g_tc_last_name_string;
                static DDS_TypeCode user_g_tc_group_name_string;

                static DDS_TypeCode_Member user_g_tc_members[4]=
                {

                    {
                        (char *)"user_id",/* Member name */
                        {
                            0,/* Representation ID */
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_KEY_MEMBER , /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL, /* Ignored */
                        RTICdrTypeCodeAnnotations_INITIALIZER
                    }, 
                    {
                        (char *)"first_name",/* Member name */
                        {
                            1,/* Representation ID */
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL, /* Ignored */
                        RTICdrTypeCodeAnnotations_INITIALIZER
                    }, 
                    {
                        (char *)"last_name",/* Member name */
                        {
                            2,/* Representation ID */
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL, /* Ignored */
                        RTICdrTypeCodeAnnotations_INITIALIZER
                    }, 
                    {
                        (char *)"group_name",/* Member name */
                        {
                            3,/* Representation ID */
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL, /* Ignored */
                        RTICdrTypeCodeAnnotations_INITIALIZER
                    }
                };

                static DDS_TypeCode user_g_tc =
                {{
                        DDS_TK_STRUCT, /* Kind */
                        DDS_BOOLEAN_FALSE, /* Ignored */
                        -1, /*Ignored*/
                        (char *)"user", /* Name */
                        NULL, /* Ignored */      
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        4, /* Number of members */
                        user_g_tc_members, /* Members */
                        DDS_VM_NONE, /* Ignored */
                        RTICdrTypeCodeAnnotations_INITIALIZER,
                        DDS_BOOLEAN_TRUE, /* _isCopyable */
                        NULL, /* _sampleAccessInfo: assigned later */
                        NULL /* _typePlugin: assigned later */
                    }}; /* Type code for user*/

                if (is_initialized) {
                    return &user_g_tc;
                }

                user_g_tc_user_id_string = initialize_string_typecode(((MAX_NAME_LEN)));
                user_g_tc_first_name_string = initialize_string_typecode(((MAX_NAME_LEN)));
                user_g_tc_last_name_string = initialize_string_typecode(((MAX_NAME_LEN)));
                user_g_tc_group_name_string = initialize_string_typecode(((MAX_NAME_LEN)));

                user_g_tc._data._annotations._allowedDataRepresentationMask = 5;

                user_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&user_g_tc_user_id_string;
                user_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&user_g_tc_first_name_string;
                user_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&user_g_tc_last_name_string;
                user_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)&user_g_tc_group_name_string;

                /* Initialize the values for member annotations. */
                user_g_tc_members[0]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
                user_g_tc_members[0]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

                user_g_tc_members[1]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
                user_g_tc_members[1]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

                user_g_tc_members[2]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
                user_g_tc_members[2]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

                user_g_tc_members[3]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
                user_g_tc_members[3]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

                user_g_tc._data._sampleAccessInfo = sample_access_info();
                user_g_tc._data._typePlugin = type_plugin_info();    

                is_initialized = RTI_TRUE;

                return &user_g_tc;
            }

            static RTIXCdrSampleAccessInfo * sample_access_info()
            {
                static RTIBool is_initialized = RTI_FALSE;

                user *sample;

                static RTIXCdrMemberAccessInfo user_g_memberAccessInfos[4] =
                {RTIXCdrMemberAccessInfo_INITIALIZER};

                static RTIXCdrSampleAccessInfo user_g_sampleAccessInfo = 
                RTIXCdrSampleAccessInfo_INITIALIZER;

                if (is_initialized) {
                    return (RTIXCdrSampleAccessInfo*) &user_g_sampleAccessInfo;
                }

                RTIXCdrHeap_allocateStruct(
                    &sample, 
                    user);
                if (sample == NULL) {
                    return NULL;
                }

                user_g_memberAccessInfos[0].bindingMemberValueOffset[0] = 
                (RTIXCdrUnsignedLong) ((char *)&sample->user_id() - (char *)sample);

                user_g_memberAccessInfos[1].bindingMemberValueOffset[0] = 
                (RTIXCdrUnsignedLong) ((char *)&sample->first_name() - (char *)sample);

                user_g_memberAccessInfos[2].bindingMemberValueOffset[0] = 
                (RTIXCdrUnsignedLong) ((char *)&sample->last_name() - (char *)sample);

                user_g_memberAccessInfos[3].bindingMemberValueOffset[0] = 
                (RTIXCdrUnsignedLong) ((char *)&sample->group_name() - (char *)sample);

                user_g_sampleAccessInfo.memberAccessInfos = 
                user_g_memberAccessInfos;

                {
                    size_t candidateTypeSize = sizeof(user);

                    if (candidateTypeSize > RTIXCdrLong_MAX) {
                        user_g_sampleAccessInfo.typeSize[0] =
                        RTIXCdrLong_MAX;
                    } else {
                        user_g_sampleAccessInfo.typeSize[0] =
                        (RTIXCdrUnsignedLong) candidateTypeSize;
                    }
                }

                user_g_sampleAccessInfo.useGetMemberValueOnlyWithRef =
                RTI_XCDR_TRUE;

                user_g_sampleAccessInfo.getMemberValuePointerFcn = 
                interpreter::get_aggregation_value_pointer< user >;

                user_g_sampleAccessInfo.languageBinding = 
                RTI_XCDR_TYPE_BINDING_CPP_11_STL ;

                RTIXCdrHeap_freeStruct(sample);
                is_initialized = RTI_TRUE;
                return (RTIXCdrSampleAccessInfo*) &user_g_sampleAccessInfo;
            }

            static RTIXCdrTypePlugin * type_plugin_info()
            {
                static RTIXCdrTypePlugin user_g_typePlugin = 
                {
                    NULL, /* serialize */
                    NULL, /* serialize_key */
                    NULL, /* deserialize_sample */
                    NULL, /* deserialize_key_sample */
                    NULL, /* skip */
                    NULL, /* get_serialized_sample_size */
                    NULL, /* get_serialized_sample_max_size_ex */
                    NULL, /* get_serialized_key_max_size_ex */
                    NULL, /* get_serialized_sample_min_size */
                    NULL, /* serialized_sample_to_key */
                    NULL,
                    NULL,
                    NULL,
                    NULL
                };

                return &user_g_typePlugin;
            }
        }; // native_type_code
        #endif

        const ::dds::core::xtypes::StructType& dynamic_type< user >::get()
        {
            return static_cast<const ::dds::core::xtypes::StructType&>(
                ::rti::core::native_conversions::cast_from_native< ::dds::core::xtypes::DynamicType >(
                    *(native_type_code< user >::get())));
        }

        #ifndef NDDS_STANDALONE_TYPE
        template<>
        struct native_type_code< message > {
            static DDS_TypeCode * get()
            {
                using namespace ::rti::topic::interpreter;

                static RTIBool is_initialized = RTI_FALSE;

                static DDS_TypeCode message_g_tc_to_id_string;
                static DDS_TypeCode message_g_tc_from_id_string;
                static DDS_TypeCode message_g_tc_msg_string;

                static DDS_TypeCode_Member message_g_tc_members[3]=
                {

                    {
                        (char *)"to_id",/* Member name */
                        {
                            0,/* Representation ID */
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_KEY_MEMBER , /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL, /* Ignored */
                        RTICdrTypeCodeAnnotations_INITIALIZER
                    }, 
                    {
                        (char *)"from_id",/* Member name */
                        {
                            1,/* Representation ID */
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL, /* Ignored */
                        RTICdrTypeCodeAnnotations_INITIALIZER
                    }, 
                    {
                        (char *)"msg",/* Member name */
                        {
                            2,/* Representation ID */
                            DDS_BOOLEAN_FALSE,/* Is a pointer? */
                            -1, /* Bitfield bits */
                            NULL/* Member type code is assigned later */
                        },
                        0, /* Ignored */
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
                        DDS_PUBLIC_MEMBER,/* Member visibility */
                        1,
                        NULL, /* Ignored */
                        RTICdrTypeCodeAnnotations_INITIALIZER
                    }
                };

                static DDS_TypeCode message_g_tc =
                {{
                        DDS_TK_STRUCT, /* Kind */
                        DDS_BOOLEAN_FALSE, /* Ignored */
                        -1, /*Ignored*/
                        (char *)"message", /* Name */
                        NULL, /* Ignored */      
                        0, /* Ignored */
                        0, /* Ignored */
                        NULL, /* Ignored */
                        3, /* Number of members */
                        message_g_tc_members, /* Members */
                        DDS_VM_NONE, /* Ignored */
                        RTICdrTypeCodeAnnotations_INITIALIZER,
                        DDS_BOOLEAN_TRUE, /* _isCopyable */
                        NULL, /* _sampleAccessInfo: assigned later */
                        NULL /* _typePlugin: assigned later */
                    }}; /* Type code for message*/

                if (is_initialized) {
                    return &message_g_tc;
                }

                message_g_tc_to_id_string = initialize_string_typecode(((MAX_NAME_LEN)));
                message_g_tc_from_id_string = initialize_string_typecode(((MAX_NAME_LEN)));
                message_g_tc_msg_string = initialize_string_typecode(((MAX_MSG_LEN)));

                message_g_tc._data._annotations._allowedDataRepresentationMask = 5;

                message_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&message_g_tc_to_id_string;
                message_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&message_g_tc_from_id_string;
                message_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&message_g_tc_msg_string;

                /* Initialize the values for member annotations. */
                message_g_tc_members[0]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
                message_g_tc_members[0]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

                message_g_tc_members[1]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
                message_g_tc_members[1]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

                message_g_tc_members[2]._annotations._defaultValue._d = RTI_XCDR_TK_STRING;
                message_g_tc_members[2]._annotations._defaultValue._u.string_value = (DDS_Char *) "";

                message_g_tc._data._sampleAccessInfo = sample_access_info();
                message_g_tc._data._typePlugin = type_plugin_info();    

                is_initialized = RTI_TRUE;

                return &message_g_tc;
            }

            static RTIXCdrSampleAccessInfo * sample_access_info()
            {
                static RTIBool is_initialized = RTI_FALSE;

                message *sample;

                static RTIXCdrMemberAccessInfo message_g_memberAccessInfos[3] =
                {RTIXCdrMemberAccessInfo_INITIALIZER};

                static RTIXCdrSampleAccessInfo message_g_sampleAccessInfo = 
                RTIXCdrSampleAccessInfo_INITIALIZER;

                if (is_initialized) {
                    return (RTIXCdrSampleAccessInfo*) &message_g_sampleAccessInfo;
                }

                RTIXCdrHeap_allocateStruct(
                    &sample, 
                    message);
                if (sample == NULL) {
                    return NULL;
                }

                message_g_memberAccessInfos[0].bindingMemberValueOffset[0] = 
                (RTIXCdrUnsignedLong) ((char *)&sample->to_id() - (char *)sample);

                message_g_memberAccessInfos[1].bindingMemberValueOffset[0] = 
                (RTIXCdrUnsignedLong) ((char *)&sample->from_id() - (char *)sample);

                message_g_memberAccessInfos[2].bindingMemberValueOffset[0] = 
                (RTIXCdrUnsignedLong) ((char *)&sample->msg() - (char *)sample);

                message_g_sampleAccessInfo.memberAccessInfos = 
                message_g_memberAccessInfos;

                {
                    size_t candidateTypeSize = sizeof(message);

                    if (candidateTypeSize > RTIXCdrLong_MAX) {
                        message_g_sampleAccessInfo.typeSize[0] =
                        RTIXCdrLong_MAX;
                    } else {
                        message_g_sampleAccessInfo.typeSize[0] =
                        (RTIXCdrUnsignedLong) candidateTypeSize;
                    }
                }

                message_g_sampleAccessInfo.useGetMemberValueOnlyWithRef =
                RTI_XCDR_TRUE;

                message_g_sampleAccessInfo.getMemberValuePointerFcn = 
                interpreter::get_aggregation_value_pointer< message >;

                message_g_sampleAccessInfo.languageBinding = 
                RTI_XCDR_TYPE_BINDING_CPP_11_STL ;

                RTIXCdrHeap_freeStruct(sample);
                is_initialized = RTI_TRUE;
                return (RTIXCdrSampleAccessInfo*) &message_g_sampleAccessInfo;
            }

            static RTIXCdrTypePlugin * type_plugin_info()
            {
                static RTIXCdrTypePlugin message_g_typePlugin = 
                {
                    NULL, /* serialize */
                    NULL, /* serialize_key */
                    NULL, /* deserialize_sample */
                    NULL, /* deserialize_key_sample */
                    NULL, /* skip */
                    NULL, /* get_serialized_sample_size */
                    NULL, /* get_serialized_sample_max_size_ex */
                    NULL, /* get_serialized_key_max_size_ex */
                    NULL, /* get_serialized_sample_min_size */
                    NULL, /* serialized_sample_to_key */
                    NULL,
                    NULL,
                    NULL,
                    NULL
                };

                return &message_g_typePlugin;
            }
        }; // native_type_code
        #endif

        const ::dds::core::xtypes::StructType& dynamic_type< message >::get()
        {
            return static_cast<const ::dds::core::xtypes::StructType&>(
                ::rti::core::native_conversions::cast_from_native< ::dds::core::xtypes::DynamicType >(
                    *(native_type_code< message >::get())));
        }

    }
}

namespace dds { 
    namespace topic {
        void topic_type_support< user >:: register_type(
            ::dds::domain::DomainParticipant& participant,
            const std::string& type_name) 
        {

            ::rti::domain::register_type_plugin(
                participant,
                type_name,
                userPlugin_new,
                userPlugin_delete);
        }

        std::vector<char>& topic_type_support< user >::to_cdr_buffer(
            std::vector<char>& buffer, 
            const user& sample,
            ::dds::core::policy::DataRepresentationId representation)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = userPlugin_serialize_to_cdr_buffer(
                NULL, 
                &length,
                &sample,
                representation);
            ::rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = userPlugin_serialize_to_cdr_buffer(
                &buffer[0], 
                &length, 
                &sample,
                representation);
            ::rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;
        }

        void topic_type_support< user >::from_cdr_buffer(user& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = userPlugin_deserialize_from_cdr_buffer(
                &sample, 
                &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            ::rti::core::check_return_code(ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
            "Failed to create user from cdr buffer");
        }

        void topic_type_support< user >::reset_sample(user& sample) 
        {
            sample.user_id("");
            sample.first_name("");
            sample.last_name("");
            sample.group_name("");
        }

        void topic_type_support< user >::allocate_sample(user& sample, int, int) 
        {
            ::rti::topic::allocate_sample(sample.user_id(),  -1, (MAX_NAME_LEN));
            ::rti::topic::allocate_sample(sample.first_name(),  -1, (MAX_NAME_LEN));
            ::rti::topic::allocate_sample(sample.last_name(),  -1, (MAX_NAME_LEN));
            ::rti::topic::allocate_sample(sample.group_name(),  -1, (MAX_NAME_LEN));
        }

        void topic_type_support< message >:: register_type(
            ::dds::domain::DomainParticipant& participant,
            const std::string& type_name) 
        {

            ::rti::domain::register_type_plugin(
                participant,
                type_name,
                messagePlugin_new,
                messagePlugin_delete);
        }

        std::vector<char>& topic_type_support< message >::to_cdr_buffer(
            std::vector<char>& buffer, 
            const message& sample,
            ::dds::core::policy::DataRepresentationId representation)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = messagePlugin_serialize_to_cdr_buffer(
                NULL, 
                &length,
                &sample,
                representation);
            ::rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = messagePlugin_serialize_to_cdr_buffer(
                &buffer[0], 
                &length, 
                &sample,
                representation);
            ::rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;
        }

        void topic_type_support< message >::from_cdr_buffer(message& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = messagePlugin_deserialize_from_cdr_buffer(
                &sample, 
                &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            ::rti::core::check_return_code(ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
            "Failed to create message from cdr buffer");
        }

        void topic_type_support< message >::reset_sample(message& sample) 
        {
            sample.to_id("");
            sample.from_id("");
            sample.msg("");
        }

        void topic_type_support< message >::allocate_sample(message& sample, int, int) 
        {
            ::rti::topic::allocate_sample(sample.to_id(),  -1, (MAX_NAME_LEN));
            ::rti::topic::allocate_sample(sample.from_id(),  -1, (MAX_NAME_LEN));
            ::rti::topic::allocate_sample(sample.msg(),  -1, (MAX_MSG_LEN));
        }

    }
}  

