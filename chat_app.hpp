

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from chat_app.idl
using RTI Code Generator (rtiddsgen) version 3.1.0.
The rtiddsgen tool is part of the RTI Connext DDS distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the Code Generator User's Manual.
*/

#ifndef chat_app_1835167378_hpp
#define chat_app_1835167378_hpp

#include <iosfwd>

#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport __declspec(dllexport)
#endif

#include "dds/domain/DomainParticipant.hpp"
#include "dds/topic/TopicTraits.hpp"
#include "dds/core/SafeEnumeration.hpp"
#include "dds/core/String.hpp"
#include "dds/core/array.hpp"
#include "dds/core/vector.hpp"
#include "dds/core/Optional.hpp"
#include "dds/core/xtypes/DynamicType.hpp"
#include "dds/core/xtypes/StructType.hpp"
#include "dds/core/xtypes/UnionType.hpp"
#include "dds/core/xtypes/EnumType.hpp"
#include "dds/core/xtypes/AliasType.hpp"
#include "rti/core/array.hpp"
#include "rti/core/BoundedSequence.hpp"
#include "rti/util/StreamFlagSaver.hpp"
#include "rti/domain/PluginSupport.hpp"
#include "rti/core/LongDouble.hpp"
#include "dds/core/External.hpp"
#include "rti/core/Pointer.hpp"
#include "rti/topic/TopicTraits.hpp"

#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

static const uint16_t MAX_NAME_LEN = 32U;

static const uint16_t MAX_MSG_LEN = 160U;

class NDDSUSERDllExport user {
  public:
    user();

    user(
        const std::string& user_id,
        const std::string& first_name,
        const std::string& last_name,
        const std::string& group_name);

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    user (user&&) = default;
    user& operator=(user&&) = default;
    user& operator=(const user&) = default;
    user(const user&) = default;
    #else
    user(user&& other_) OMG_NOEXCEPT;  
    user& operator=(user&&  other_) OMG_NOEXCEPT;
    #endif
    #endif 

    std::string& user_id() OMG_NOEXCEPT {
        return m_user_id_;
    }

    const std::string& user_id() const OMG_NOEXCEPT {
        return m_user_id_;
    }

    void user_id(const std::string& value) {
        m_user_id_ = value;
    }

    void user_id(std::string&& value) {
        m_user_id_ = std::move(value);
    }
    std::string& first_name() OMG_NOEXCEPT {
        return m_first_name_;
    }

    const std::string& first_name() const OMG_NOEXCEPT {
        return m_first_name_;
    }

    void first_name(const std::string& value) {
        m_first_name_ = value;
    }

    void first_name(std::string&& value) {
        m_first_name_ = std::move(value);
    }
    std::string& last_name() OMG_NOEXCEPT {
        return m_last_name_;
    }

    const std::string& last_name() const OMG_NOEXCEPT {
        return m_last_name_;
    }

    void last_name(const std::string& value) {
        m_last_name_ = value;
    }

    void last_name(std::string&& value) {
        m_last_name_ = std::move(value);
    }
    std::string& group_name() OMG_NOEXCEPT {
        return m_group_name_;
    }

    const std::string& group_name() const OMG_NOEXCEPT {
        return m_group_name_;
    }

    void group_name(const std::string& value) {
        m_group_name_ = value;
    }

    void group_name(std::string&& value) {
        m_group_name_ = std::move(value);
    }

    bool operator == (const user& other_) const;
    bool operator != (const user& other_) const;

    void swap(user& other_) OMG_NOEXCEPT ;

  private:

    std::string m_user_id_;
    std::string m_first_name_;
    std::string m_last_name_;
    std::string m_group_name_;

};

inline void swap(user& a, user& b)  OMG_NOEXCEPT 
{
    a.swap(b);
}

NDDSUSERDllExport std::ostream& operator<<(std::ostream& o, const user& sample);

class NDDSUSERDllExport message {
  public:
    message();

    message(
        const std::string& to_id,
        const std::string& from_id,
        const std::string& msg);

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    message (message&&) = default;
    message& operator=(message&&) = default;
    message& operator=(const message&) = default;
    message(const message&) = default;
    #else
    message(message&& other_) OMG_NOEXCEPT;  
    message& operator=(message&&  other_) OMG_NOEXCEPT;
    #endif
    #endif 

    std::string& to_id() OMG_NOEXCEPT {
        return m_to_id_;
    }

    const std::string& to_id() const OMG_NOEXCEPT {
        return m_to_id_;
    }

    void to_id(const std::string& value) {
        m_to_id_ = value;
    }

    void to_id(std::string&& value) {
        m_to_id_ = std::move(value);
    }
    std::string& from_id() OMG_NOEXCEPT {
        return m_from_id_;
    }

    const std::string& from_id() const OMG_NOEXCEPT {
        return m_from_id_;
    }

    void from_id(const std::string& value) {
        m_from_id_ = value;
    }

    void from_id(std::string&& value) {
        m_from_id_ = std::move(value);
    }
    std::string& msg() OMG_NOEXCEPT {
        return m_msg_;
    }

    const std::string& msg() const OMG_NOEXCEPT {
        return m_msg_;
    }

    void msg(const std::string& value) {
        m_msg_ = value;
    }

    void msg(std::string&& value) {
        m_msg_ = std::move(value);
    }

    bool operator == (const message& other_) const;
    bool operator != (const message& other_) const;

    void swap(message& other_) OMG_NOEXCEPT ;

  private:

    std::string m_to_id_;
    std::string m_from_id_;
    std::string m_msg_;

};

inline void swap(message& a, message& b)  OMG_NOEXCEPT 
{
    a.swap(b);
}

NDDSUSERDllExport std::ostream& operator<<(std::ostream& o, const message& sample);

namespace rti {
    namespace flat {
        namespace topic {
        }
    }
}
namespace dds {
    namespace topic {

        template<>
        struct topic_type_name< user > {
            NDDSUSERDllExport static std::string value() {
                return "user";
            }
        };

        template<>
        struct is_topic_type< user > : public ::dds::core::true_type {};

        template<>
        struct topic_type_support< user > {
            NDDSUSERDllExport 
            static void register_type(
                ::dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport 
            static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, 
                const user& sample,
                ::dds::core::policy::DataRepresentationId representation 
                = ::dds::core::policy::DataRepresentation::auto_id());

            NDDSUSERDllExport 
            static void from_cdr_buffer(user& sample, const std::vector<char>& buffer);
            NDDSUSERDllExport 
            static void reset_sample(user& sample);

            NDDSUSERDllExport 
            static void allocate_sample(user& sample, int, int);

            static const ::rti::topic::TypePluginKind::type type_plugin_kind = 
            ::rti::topic::TypePluginKind::STL;
        };

        template<>
        struct topic_type_name< message > {
            NDDSUSERDllExport static std::string value() {
                return "message";
            }
        };

        template<>
        struct is_topic_type< message > : public ::dds::core::true_type {};

        template<>
        struct topic_type_support< message > {
            NDDSUSERDllExport 
            static void register_type(
                ::dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport 
            static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, 
                const message& sample,
                ::dds::core::policy::DataRepresentationId representation 
                = ::dds::core::policy::DataRepresentation::auto_id());

            NDDSUSERDllExport 
            static void from_cdr_buffer(message& sample, const std::vector<char>& buffer);
            NDDSUSERDllExport 
            static void reset_sample(message& sample);

            NDDSUSERDllExport 
            static void allocate_sample(message& sample, int, int);

            static const ::rti::topic::TypePluginKind::type type_plugin_kind = 
            ::rti::topic::TypePluginKind::STL;
        };

    }
}

namespace rti { 
    namespace topic {

        #ifndef NDDS_STANDALONE_TYPE
        template<>
        struct dynamic_type< user > {
            typedef ::dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const ::dds::core::xtypes::StructType& get();
        };
        #endif

        template <>
        struct extensibility< user > {
            static const ::dds::core::xtypes::ExtensibilityKind::type kind =
            ::dds::core::xtypes::ExtensibilityKind::EXTENSIBLE;                
        };

        #ifndef NDDS_STANDALONE_TYPE
        template<>
        struct dynamic_type< message > {
            typedef ::dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const ::dds::core::xtypes::StructType& get();
        };
        #endif

        template <>
        struct extensibility< message > {
            static const ::dds::core::xtypes::ExtensibilityKind::type kind =
            ::dds::core::xtypes::ExtensibilityKind::EXTENSIBLE;                
        };

    }
}

#if (defined(RTI_WIN32) || defined (RTI_WINCE) || defined(RTI_INTIME)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif // chat_app_1835167378_hpp

